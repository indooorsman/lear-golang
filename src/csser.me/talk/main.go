package main

import (
  "fmt"
  "net/http"
  "csser.me/talk/routes"
)

func main() {
  fmt.Println("let's talk!")
  
  http.ListenAndServe(":8081", routes.InitRouter())
}


