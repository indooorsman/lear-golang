package routes

import (
  "net/http"
  "github.com/julienschmidt/httprouter"
  "html/template"
  "os"
  "path/filepath"
)

type indexPage struct {
  Title string
}

func InitRouter() *httprouter.Router {
  r := httprouter.New()
  cwd, _ := os.Getwd()
  indexPageFile := filepath.Join(cwd, "./views/hello.html")
  r.GET("/", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    t, _ := template.ParseFiles(indexPageFile)
    t.Execute(w, indexPage{Title: "你好，Go！"})
  })
  return r
}
